import {ThemeProvider} from '@emotion/react';
import './App.css';
import {ExplorerProvider} from './components/windows/ExplorerContext';
import {theme} from './theme/dark';
import WrappedApp from './WrappedApp';

function App() {

    return (
        <div className="App">
            <ThemeProvider theme={theme}>
                <ExplorerProvider>
                    <WrappedApp/>
                </ExplorerProvider>
            </ThemeProvider>
        </div>
    );
}

export default App;
