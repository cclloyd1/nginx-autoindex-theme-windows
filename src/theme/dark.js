
export const theme = {
    colors: {
        background: {
            main: '#202020',
            dark: '#191919',
        },
        border: {
            translucent: 'rgba(255,255,255,.12)', // 83
            main: '#535353', // 83
            dark: '#404040', // 64
        },
        text: {
            darker: '#808080', // 128
            dark: '#8c8c8c', // 140
            main: '#ffffff', // 255
        },
        scrollbar: '#4d4d4d', // 77
        icons: {
            main: '#95a0a6', // 149,160,166
        }
    }
}
