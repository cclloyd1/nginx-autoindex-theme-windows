
export const getFileIcon = (file) => {
    const ext = file.name.split('.').slice(-1);
    switch (ext) {
        case 'txt': return '/icons/shell32/shell32-71.ico';
        default: return '/icons/imageres/imageres-3.ico';
    }
}