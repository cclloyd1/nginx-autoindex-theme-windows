import {Resizable} from 're-resizable';
import {useEffect, useRef, useState} from 'react';
import Draggable from 'react-draggable';
import './App.css';
import {useExplorer} from './components/windows/ExplorerContext';
import ExplorerWindow from './components/windows/ExplorerWindow';


export default function WrappedApp() {

    const { sizeX, setSizeX, sizeY, setSizeY, posX, posY, setPosX, setPosY, getFilesFromIndex, fileTree, setFileTree } = useExplorer();

    const [oSizeX, setOSizeX] = useState(sizeX);
    const [oSizeY, setOSizeY] = useState(sizeY);
    const [oPosX, setOPosX] = useState(posX);
    const [oPosY, setOPosY] = useState(posY);

    const ref = useRef(null);

    const handleResizeStart = () => {
        setOSizeX(sizeX);
        setOSizeY(sizeY);
        setOPosX(posX);
        setOPosY(posY);
    }

    const handleResize = (e, direction, ref, d) => {

        const handleLeft = () => {
            setPosX(Math.max(0, oPosX - d.width));
            if (posX > 0 && e.clientX >= 0) setSizeX(oSizeX + d.width);
            else if (posX > 0 && e.clientX < 0) setSizeX(oSizeX + (d.width - Math.abs(e.clientX)));
        }

        const handleTop = () => {
            setPosY(Math.max(0, oPosY - d.height));
            if (posY > 0 && e.clientY >= 0) setSizeY(oSizeY + d.height);
            else if (posY > 0 && e.clientY < 0) setSizeY(oSizeY + (d.height - Math.abs(e.clientY)));
        }

        const handleBottom = () => setSizeY(oSizeY + d.height);
        const handleRight = () => setSizeX(oSizeX + d.width);

        setSizeX(oSizeX + d.width);
        switch (direction) {
            case 'top':
                handleTop();
                break;
            case 'left':
                handleLeft();
                break;
            case 'right':
                handleRight();
                break;
            case 'bottom':
                handleBottom();
                break;
            case 'topLeft':
                handleTop();
                handleLeft();
                break;
            case 'topRight':
                handleTop();
                handleRight();
                break;
            case 'bottomLeft':
                handleBottom();
                handleLeft();
                break;
            case 'bottomRight':
                handleBottom();
                handleRight();
                break;
            default:
                break;
        }
    }

    const handleDrag = (event, info) => {
        setPosX(info.x)
        setPosY(info.y)
    }

    useEffect(() => {
        setFileTree([...fileTree, {
            files: getFilesFromIndex(document.documentElement.outerHTML),
            active: null,
            name: '/',
        }]);
    }, []);

    return (
        <Draggable
            bounds={'body'}
            handle={'.dragHandle'}
            onDrag={handleDrag}
            defaultPosition={{x: posX, y: posY}}
            position={{x: posX, y: posY}}
        >
            <Resizable
                defaultSize={{width: sizeX, height: sizeY}}
                size={{width: sizeX, height: sizeY}}
                onResizeStart={handleResizeStart}
                onResize={handleResize}
                bound={'body'}
            >
                <ExplorerWindow/>
            </Resizable>
        </Draggable>
    );
}

