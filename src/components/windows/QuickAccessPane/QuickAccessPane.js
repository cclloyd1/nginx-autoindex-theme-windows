import styled from '@emotion/styled'


const StyledDiv = styled.div`
  min-width: 200px;
  height: 100%;
  width: 200px;
  background-color: ${props => props.theme.colors.background.dark};
  border-right: 1px solid #2b2b2b;
`

function QuickAccessPane(props) {

    return (
        <StyledDiv>
        </StyledDiv>
    );

}

export default QuickAccessPane;