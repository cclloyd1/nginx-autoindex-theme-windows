import React, {Component, createContext, useContext, useState} from 'react';

const ExplorerContext = createContext(undefined);

export function ExplorerProvider({children}) {
    const [fileTree, setFileTree] = useState([]);
    const [sizeX, setSizeX] = useState(1000);
    const [sizeY, setSizeY] = useState(600);
    const [loading, setLoading] = useState(false);

    const [posX, setPosX] = useState((window.innerWidth / 2) - (sizeX / 2));
    const [posY, setPosY] = useState((window.innerHeight / 2) - (sizeY / 2));

    const [savedPos, setSavedPos] = useState(null);

    const [sort, setSort] = useState('name');
    const [sortDir, setSortDir] = useState('asc');

    const [menuActive, setMenuActive] = useState(null);
    const [dropActive, setDropActive] = useState(null);

    const [activeMenu, setActiveMenu] = useState(null);

    const toggleLoading = () => {
        setLoading(!loading);
    }



    const handleSort = (sortID) => {
        if (sortID === sort) {
            if (sortDir === 'asc') setSortDir('desc');
            else setSortDir('asc');
        }
        else {
            setSort(sortID);
            setSortDir('asc');
        }

        const asc = sortDir === 'asc';

        const old = [...fileTree];
        switch (sortID) {
            case 'name':
                old.at(-1).files.sort((a, b) => {
                    if (!asc) {
                        if (a.isDir && !b.isDir) return 1;
                        else if (b.isDir && !a.isDir) return -1;
                        return a.name.localeCompare(b.name);
                    }
                    if (b.isDir && !a.isDir) return 1;
                    else if (a.isDir && !b.isDir) return -1;
                    return b.name.localeCompare(a.name);
                });
                break;
            case 'size':
                old.at(-1).files.sort((a, b) => {
                    if (!asc) {
                        if (a.isDir && !b.isDir) return 1;
                        else if (b.isDir && !a.isDir) return -1;
                        return a.size > b.size;
                    }
                    if (b.isDir && !a.isDir) return 1;
                    else if (a.isDir && !b.isDir) return -1;
                    return a.size < b.size;
                });
                break;
            case 'dateModified':
                old.at(-1).files.sort((a, b) => {
                    if (!asc) {
                        if (a.isDir && !b.isDir) return 1;
                        else if (b.isDir && !a.isDir) return -1;
                        return a.date.localeCompare(b.date);
                    }
                    if (b.isDir && !a.isDir) return 1;
                    else if (a.isDir && !b.isDir) return -1;
                    return b.date.localeCompare(a.date);
                });
                break;
            default: break;
        }
        setFileTree(old);

    }

    const getFilesFromIndex = (data) => {
        const doc = new DOMParser().parseFromString(data, "text/html")

        try {
            const elem = doc.getElementById('list').getElementsByTagName('tbody')[0];
            //console.log('Elem', elem)
            const allFiles = [];
            for (let e of elem.children) {
                const row = {};
                row.url = e.children[0].firstChild.href;
                row.name = e.children[0].firstChild.innerHTML;
                row.size = parseInt(e.children[1].innerHTML);
                row.date = e.children[2].innerHTML;

                if (row.name === 'Parent directory/')
                    row.name = '../'
                if (row.name.endsWith('/')){
                    row.isDir = true;
                    row.name = row.name.slice(0, -1);
                    row.size = -1;
                }

                allFiles.push(row);
            }
            return allFiles;
        } catch (error) {
            console.error('Error parsing fileTree', error);
        }
    }

    return (
        <ExplorerContext.Provider value={{
            fileTree, setFileTree,
            sizeX, sizeY, setSizeX, setSizeY, 
            posX, posY, setPosX, setPosY,
            sort, setSort, handleSort, sortDir, setSortDir,
            getFilesFromIndex,
            menuActive, setMenuActive,
            savedPos, setSavedPos,
            dropActive, setDropActive,
            activeMenu, setActiveMenu,
            loading, setLoading, toggleLoading,
        }}>
            {children}
        </ExplorerContext.Provider>
    )
}

export const useExplorer = () => {
    const ctx = useContext(ExplorerContext);

    if (!ctx) throw Error('The `useExplorer` hook must be called from a descendent of the `ExplorerProvider`.');

    return {
        menuActive: ctx.menuActive, setMenuActive: ctx.setMenuActive,
        addRow: ctx.addRow,
        fileTree: ctx.fileTree, setFileTree: ctx.setFileTree,
        sizeX: ctx.sizeX, sizeY: ctx.sizeY, setSizeX: ctx.setSizeX, setSizeY: ctx.setSizeY,
        posX: ctx.posX, posY: ctx.posY, setPosX: ctx.setPosX, setPosY: ctx.setPosY,
        getFilesFromIndex: ctx.getFilesFromIndex,
        sort: ctx.sort, setSort: ctx.setSort, handleSort: ctx.handleSort,
        sortDir: ctx.sortDir, setSortDir: ctx.setSortDir,
        savedPos: ctx.savedPos, setSavedPos: ctx.setSavedPos,
        dropActive: ctx.dropActive, setDropActive: ctx.setDropActive,
        activeMenu: ctx.activeMenu, setActiveMenu: ctx.setActiveMenu,
        loading: ctx.loading, setLoading: ctx.setLoading, toggleLoading: ctx.toggleLoading,
    }
}