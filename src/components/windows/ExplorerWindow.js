import styled from '@emotion/styled'
import {useEffect} from 'react';
import ContentPane from './ContentPane/ContentPane';
import {useExplorer} from './ExplorerContext';
import NavRow from './NavRow/NavRow';
import QuickAccessPane from './QuickAccessPane/QuickAccessPane';
import WindowTop from './TopBar/WindowTop';


const StyledWindow = styled.div`
  min-width: 200px;
  min-height: 200px;
  height: ${props => props.sizeY}px;
  width: ${props => props.sizeX}px;
  background-color: ${props => props.theme.colors.background.main};
  border: 1px solid ${props => props.theme.colors.border.translucent};
  overflow: visible;
  display: flex;
  flex-direction: column;
  ${props => props.loading && `
    cursor: wait;
  `},
`


const StyledTop = styled.div`
  width: 100%;
`

const StyledRight = styled.div`
  width: 100%;
`

const StyledContent = styled.div`
  width: 100%;
  display: flex;
  flex-grow: 1;
  overflow: hidden;
`

export default function ExplorerWindow() {

    const { sizeX, sizeY, menuActive, setMenuActive, loading, activeMenu, setActiveMenu } = useExplorer();

    useEffect(() => {
        document.querySelectorAll('.explorerWindow').forEach(e =>
            e.addEventListener('contextmenu', event => event.preventDefault())
        );
    }, []);

    const handleClick = (e) => {
        if (menuActive) setMenuActive(null);
        if (activeMenu) setActiveMenu(null);
        // TODO: Swap old menuActive (FileRow) to new activeMenu (PathPiece)
    }

    return (
        <StyledWindow
            loading={loading}
            sizeX={sizeX}
            sizeY={sizeY}
            className={'explorerWindow'}
            onClick={handleClick}
        >
            <StyledTop>
                <WindowTop/>
            </StyledTop>

            <NavRow/>

            <StyledContent>
                <QuickAccessPane/>

                <StyledRight>
                    <ContentPane/>
                </StyledRight>
            </StyledContent>

        </StyledWindow>
    );
}

