import styled from '@emotion/styled'
import {faChevronDown, faChevronUp, faXmark} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useState} from 'react';
import WindowCloseButton from '../TitlePane/WindowCloseButton';


const StyledDiv = styled.div`
  width: 100%;
`

const ControlBar = styled.div`
  height: 26px;
  width: 100%;
  background-color: black;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const LeftControl = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`

const RightControl = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`

const StyledContent = styled.div`
  height: ${props => props.collapsed ? 0 : 91}px;
  width: 100%;
  background-color: ${props => props.theme.colors.background.main};
`


const Icon = styled.div`
  width: 22px;
  height: 22px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  &:hover {
    background-color: #434343;
    border: 1px solid #313131;
  }
  &:active {
    background-color: #686868;
    border: 1px solid #636363;
  }
`

const CollapseIcon = styled(FontAwesomeIcon)`
  font-size: 10px;
  color: #a2a2a2;
`

const HelpIcon = styled('img')`
  width: 16px;
  height: 16px;
`

export default function RibbonPane() {
    const [collapsed, setCollapsed] = useState(true);

    const handleCollapseClick = () => {
        setCollapsed(!collapsed);
    }

    const handleHelpClick = () => {
        console.log('Unimplemented');
    }

    return (
        <StyledDiv>
            <ControlBar>
                <LeftControl>
                </LeftControl>

                <RightControl>
                    <Icon onClick={handleCollapseClick}><CollapseIcon icon={collapsed ? faChevronDown : faChevronUp}/></Icon>
                    <Icon onClick={handleHelpClick}><HelpIcon src={'/icons/shell32/shell32-24.ico'}/></Icon>
                </RightControl>
            </ControlBar>


            <StyledContent collapsed={collapsed}>
            </StyledContent>
        </StyledDiv>
    );
}


