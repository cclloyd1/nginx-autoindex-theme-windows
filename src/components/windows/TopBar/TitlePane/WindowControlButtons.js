import styled from '@emotion/styled'
import WindowCloseButton from './WindowCloseButton';
import WindowMaximizeButton from './WindowMaximizeButton';
import WindowMinimizeButton from './WindowMinimizeButton';


const StyledWindowControlButtons = styled('div')`
  display: flex;
  justify-content: right;
  align-items: center;
  width: 135px;
`

function WindowControlButtons(props) {

    return (
        <StyledWindowControlButtons>
            <WindowMinimizeButton/>
            <WindowMaximizeButton/>
            <WindowCloseButton/>
        </StyledWindowControlButtons>
    );

}

export default WindowControlButtons;