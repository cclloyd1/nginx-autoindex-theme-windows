import styled from '@emotion/styled'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faWindowMinimize} from '@fortawesome/free-solid-svg-icons';
import WindowButtonControlBase from './WindowButtonControlBase';



const StyledWindowMinimizeButton = styled(WindowButtonControlBase)`
`

function WindowMinimizeButton(props) {

    return (
        <StyledWindowMinimizeButton>
            <FontAwesomeIcon icon={faWindowMinimize} width={'10px'}/>
        </StyledWindowMinimizeButton>
    );

}

export default WindowMinimizeButton;