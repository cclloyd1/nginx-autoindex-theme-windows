import styled from '@emotion/styled'
import WindowControlButtons from './WindowControlButtons';


const StyledWindowTitleBar = styled('div')`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: black;
`

const StyledWindowName = styled('div')`
  display: inline-block;
  user-select: none;
  padding-left: 1em;
  line-height: 28px;
  font-family: 'Calibri', sans-serif;
  font-size: 11pt;
  color: ${props => props.theme.colors.text.main};
`

const StyledDragHandle = styled('div')`
  display: inline-block;
  flex-grow: 1;
  height: 28px;
`

function WindowTitleBar({title}) {

    return (
        <StyledWindowTitleBar>
            <StyledWindowName>{title}</StyledWindowName>
            <StyledDragHandle className={'dragHandle'}/>
            <WindowControlButtons/>
        </StyledWindowTitleBar>
    );

}

export default WindowTitleBar;