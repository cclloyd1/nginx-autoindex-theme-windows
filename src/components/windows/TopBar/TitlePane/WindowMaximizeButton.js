import styled from '@emotion/styled'
import {faSquareFull} from '@fortawesome/free-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useState} from 'react';
import {useExplorer} from '../../ExplorerContext';
import WindowButtonControlBase from './WindowButtonControlBase';



const StyledWindowMaximizeButton = styled(WindowButtonControlBase)`
`

function WindowMaximizeButton(props) {
    const size = '10px'

    const [maximized, setMaximized] = useState(false);

    const { sizeX, sizeY, posX, posY, setSizeX, setSizeY, setPosX, setPosY, savedPos, setSavedPos } = useExplorer();

    // TODO: in ExplorerContext, save previous window position before maximized
    const handleClick = (e) => {
        if (maximized) {
            setMaximized(false);
            setPosX(savedPos.x);
            setPosY(savedPos.y);
            setSizeX(savedPos.width);
            setSizeY(savedPos.height);
        }
        else {
            setSizeX(window.innerWidth);
            setSizeY(window.window.innerHeight);
            setPosX(0);
            setPosY(0);
            setMaximized(true);
            setSavedPos({
                width: sizeX,
                height: sizeY,
                x: posX,
                y: posY,
            });
        }
    }

    return (
        <StyledWindowMaximizeButton onClick={handleClick}>
            <FontAwesomeIcon icon={faSquareFull} height={size} width={size}/>
        </StyledWindowMaximizeButton>
    );

}

export default WindowMaximizeButton;