import styled from '@emotion/styled'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faXmark } from "@fortawesome/free-solid-svg-icons"
import WindowButtonControlBase from './WindowButtonControlBase';



const StyledWindowCloseButton = styled(WindowButtonControlBase)`
  &:hover {
    background-color: #e81123;
  }
  &:active {
    background-color: #8b0a14;
  }
`

function WindowCloseButton(props) {

    return (
        <StyledWindowCloseButton>
            <FontAwesomeIcon icon={faXmark} css={'height: 10px;'}/>
        </StyledWindowCloseButton>
    );

}

export default WindowCloseButton;