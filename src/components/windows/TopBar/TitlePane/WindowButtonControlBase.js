import styled from '@emotion/styled'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faXmark } from "@fortawesome/free-solid-svg-icons"



const StyledWindowButtonControlBase = styled('div')`
  //color: ${props => props.theme.colors.primary};
  color: white;
  height: 28px;
  width: 45px;
  background-color: black;
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    background-color: #1a1a1a;
  }
  &:active {
    background-color: #333333;
  }
`

function WindowButtonControlBase({children, className, ...otherProps}) {

    return (
        <StyledWindowButtonControlBase className={className} {...otherProps} >
            {children}
        </StyledWindowButtonControlBase>
    );

}

export default WindowButtonControlBase;