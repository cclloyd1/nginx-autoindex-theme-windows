import styled from '@emotion/styled'
import RibbonPane from './RibbonPane/RibbonPane';
import WindowTitleBar from './TitlePane/WindowTitleBar';


const StyledWindowTop = styled.div`
  width: 100%;
`

function WindowTop(props) {

    return (
        <StyledWindowTop>
            <WindowTitleBar title={'This PC'}/>
            <RibbonPane/>
        </StyledWindowTop>
    );

}

export default WindowTop;