import styled from '@emotion/styled'
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import NavButtonBase from './NavButtonBase';



const StyledJumplistButton = styled(NavButtonBase)`
  font-size: 10pt;
`

function JumplistButton(props) {
    const size = '7px'

    return (
        <StyledJumplistButton>
            <FontAwesomeIcon icon={faChevronDown} height={size} width={size}/>
        </StyledJumplistButton>
    );

}

export default JumplistButton;