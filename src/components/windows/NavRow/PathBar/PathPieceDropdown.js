import styled from '@emotion/styled'
import {faChevronDown, faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import axios from 'axios';
import {useEffect, useState} from 'react';
import urlJoin from 'url-join';
import {sleep} from '../../../../lib/utils';
import {useExplorer} from '../../ExplorerContext';

const StyledPathPiece = styled.div`
  height: calc(100% - 2px);
  display: flex;
  align-items: center;
  margin-right: 2px;
  user-select: none;
  position: relative;
  &:hover {
    background-color: ${props => props.hovered ? '#434343' : 'transparent'};
  }
`

const StyledPathName = styled.div`
  height: calc(100% - 2px);
  display: flex;
  align-items: center;
  padding: 0 4px;
`

const StyledPathDropdownButton = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  //border-left: 1px solid #191919;
  //margin-left: 1px;
  color: #808080;
  font-size: 8px;
  width: 14px;
`

const Dropdown = styled.div`
  min-width: 100px;
  background-color: #f2f2f2;
  position: absolute;
  top: 100%;
  right: 0;
  color: black;
  padding: 2px;
  z-index: 200;
  div {
    height: 22px;
    display: flex;
    align-items: center;
    &:hover {
      background-color: #c1dcf3;
    }
  }
`

const Sibling = styled.div`
`

const FileIcon = styled.img`
  width: 16px;
  height: auto;
  padding-right: 5px;
`

export function PathPieceDropdown({ file, index }) {
    const { fileTree, setFileTree, activeMenu, setActiveMenu, getFilesFromIndex, toggleLoading, setLoading } = useExplorer();

    const [siblings, setSiblings] = useState([]);

    useEffect(() => {
        const parent = fileTree[index];
        // Filter siblings to remove parent directories, files, and the active dir if any
        const newParent = parent.files.filter(f => {
            if (!f.isDir || f.name === '..' || f.name === '/')
                return undefined;

            if (f.name !== file.name) {
                if (fileTree[index].active === null)
                    return f;
                else if (fileTree[index].files[fileTree[index].active].name !== f.name)
                    return f;
            }
        });
        setSiblings([...newParent]);
    }, [fileTree, index]);


    const handleOptionClick = async (e, name) => {
        e.stopPropagation();
        setActiveMenu(null);

        let oldTree = [...fileTree];

        // Remove tiers up to the currently selected one
        if (index < fileTree.length - 1) {
            oldTree = oldTree.slice(0, index+1);
            oldTree.at(-1).active = null;
        }

        // Grab base URL from window
        let url = new URL(window.location).href;
        // Add all levels to URL
        oldTree.map((f, i) => {
            if (f.active !== null) url = urlJoin(url, f.files.at(f.active).name);
        })
        // Add current name that we're navigating to
        url = urlJoin(url, name);

        try {
            setLoading(true);
            // Fetch listing of next folder
            const res = await axios.get(new URL(url).href);

            // Set active to file being switched to for previous tier
            oldTree.at(-1).active = oldTree.at(-1).files.map(f => f.name).indexOf(name);

            // Update state with new file object
            setFileTree([...oldTree, {
                files: getFilesFromIndex(res.data),
                active: null,
                name: name,
            }]);
            setLoading(false);
        } catch (error) {
            console.error(error);
        }
    }

    const handleDropButtonClick = () => {
        if (activeMenu) {
            setActiveMenu(null);
        }
        else {
            setActiveMenu(prev => ({
                type: 'path',
                index: index,
                file: file,
            }));
        }
    }

    const isDropActive = activeMenu?.index === index && activeMenu?.file.name === file.name && siblings.length > 0;

    return (
        <>
            <StyledPathDropdownButton onClick={handleDropButtonClick}>
                <FontAwesomeIcon icon={isDropActive ? faChevronDown : faChevronRight} />
            </StyledPathDropdownButton>

            {isDropActive && <Dropdown>
                {siblings.map((f, i) =>
                    <Sibling key={i} onClick={(e) => handleOptionClick(e, f.name)}>
                        <FileIcon src={`/icons/shell32/shell32-5.ico`} /> {f.name}
                    </Sibling>
                )}
            </Dropdown>}
        </>
    );
}