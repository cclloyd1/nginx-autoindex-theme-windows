import styled from '@emotion/styled'
import {useEffect, useState} from 'react';
import {useExplorer} from '../../ExplorerContext';
import {PathPieceDropdown} from './PathPieceDropdown';

const StyledPathPiece = styled.div`
  height: calc(100% - 2px);
  display: flex;
  align-items: center;
  margin-right: 2px;
  user-select: none;
  position: relative;
  &:hover {
    background-color: ${props => props.hovered ? '#434343' : 'transparent'};
  }
`

const StyledPathName = styled.div`
  height: calc(100% - 2px);
  display: flex;
  align-items: center;
  padding: 0 4px;
`


export function PathPiece({ file, index }) {
    const { fileTree, setFileTree } = useExplorer();

    const [hovered, setHovered] = useState(false);

    const handleClick = () => {
        if (index < fileTree.length - 1) {
            const old = [...fileTree.slice(0, index+1)];
            old.at(-1).active = null;
            setFileTree(old);
        }
    }

    return (
        <StyledPathPiece
            hovered={hovered}
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
        >
            <StyledPathName onClick={handleClick}>
                {index !== 0 ? file.name : '/index/'}
            </StyledPathName>

            <PathPieceDropdown
                file={file}
                index={index}
            />

        </StyledPathPiece>
    );
}