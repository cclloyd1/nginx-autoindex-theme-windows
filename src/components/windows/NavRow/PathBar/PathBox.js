import styled from '@emotion/styled'
import {faRefresh} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useExplorer} from '../../ExplorerContext';
import {PathPiece} from './PathPiece';


const StyledPathBar = styled.div`
  border: 1px solid #535353;
  color: white;
  display: flex;
  height: 28px;
  flex-grow: 1;
  align-items: center;
  justify-content: space-between;
  font-size: 9pt;
`

const Pieces = styled.div`
  padding-left: 1em;
  display: flex;
  flex-grow: 1;
  align-items: center;
  justify-content: left;
  height: 100%;
`

const RefreshIconBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 28px;
  height: 100%;
  box-sizing: border-box;
  color: #808080;
  &:hover {
    border: 1px solid #227299;
    background-color: #1b2931;
  }
  &:active {
    border: 1px solid #26a0da;
    background-color: #1c3947;
  }
`

const RefreshIcon = styled(FontAwesomeIcon)`
  height: 12px;
`

export default function PathBox(props) {

    const { fileTree } = useExplorer();

    return (
        <StyledPathBar>

            <Pieces>
                {fileTree && fileTree.map((f, i) =>
                    <PathPiece key={i} file={f} index={i} />
                )}
            </Pieces>

            <RefreshIconBox>
                <RefreshIcon icon={faRefresh} />
            </RefreshIconBox>

        </StyledPathBar>
    );
}