import styled from '@emotion/styled'
import {faArrowUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useExplorer} from '../ExplorerContext';
import NavButtonBase from './NavButtonBase';



const StyledUpButton = styled(NavButtonBase)`
  font-size: 10pt;
`

function UpButton() {

    const { setFileTree, fileTree } = useExplorer();


    const handleClick = () => {
        if (fileTree.length > 1)
            setFileTree([
                ...fileTree.slice(0, -2),
                { ...fileTree.at(-2), active: null },
            ]);
    }

    return (
        <StyledUpButton onClick={handleClick} disabled={fileTree.length <= 1}>
            <FontAwesomeIcon icon={faArrowUp} />
        </StyledUpButton>
    );

}

export default UpButton;