import styled from '@emotion/styled'
import {faMagnifyingGlass} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


const StyledSearchBox = styled.div`
  border: 1px solid #535353;
  color: #808080;
  display: flex;
  height: 28px;
  min-width: 150px;
  align-items: center;
  padding: 0 16px;
  font-size: 9pt;
  margin-right: 12px;
  user-select: none;
`

const SearchIcon = styled(FontAwesomeIcon)`
  padding-right: 8px;
  height: 12px;
`

export default function SearchBox(props) {
    return (
        <StyledSearchBox>
            <SearchIcon icon={faMagnifyingGlass} />
            Search Files
        </StyledSearchBox>
    );
}