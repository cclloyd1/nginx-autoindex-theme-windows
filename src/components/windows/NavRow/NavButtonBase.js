import styled from '@emotion/styled'
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';



const StyledNavButtonBase = styled.div`
  color: ${props => props.disabled ? '#8c8c8c' : 'white'};
  display: flex;
  justify-content: center;
  align-items: center;
  width: 25px;
  height: 28px;
  font-size: 10pt;
  &:hover {
    color: ${props => props.disabled ? '#8c8c8c' : '#3298fe'};
  }
  &:active {
    color: ${props => props.disabled ? '#8c8c8c' : '#3674b2'};
  }
`

function NavButtonBase({ children, className, disabled, ...otherProps }) {
    return (
        <StyledNavButtonBase className={className} disabled={disabled} {...otherProps} >
            {children}
        </StyledNavButtonBase>
    );

}

export default NavButtonBase;