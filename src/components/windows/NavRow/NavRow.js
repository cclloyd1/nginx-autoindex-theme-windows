import styled from '@emotion/styled'
import BackButton from './BackButton';
import ForwardButton from './ForwardButton';
import JumplistButton from './JumplistButton';
import PathBox from './PathBar/PathBox';
import SearchBox from './SearchBox';
import UpButton from './UpButton';


const StyledNavRow = styled.div`
  background-color: #191919;
  height: 42px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px 0;
`

const StyledSeparator = styled.div`
  width: 12px;
  height: 28px;
  display: flex;
  justify-content: center;
`

const StyledSeparatorHandle = styled.div`
  width: 4px;
  height: 28px;
  cursor: ew-resize;
`
// TODO: small separator line between down chevron and refresh icon box

export default function NavRow(props) {

    return (
        <StyledNavRow>
            <BackButton/>
            <ForwardButton/>
            <JumplistButton/>
            <UpButton/>
            <PathBox/>
            <StyledSeparator><StyledSeparatorHandle/></StyledSeparator>
            <SearchBox/>
        </StyledNavRow>
    );

}
