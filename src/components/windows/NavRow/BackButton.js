import styled from '@emotion/styled'
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import NavButtonBase from './NavButtonBase';



const StyledBackButton = styled(NavButtonBase)`
    margin-left: 5px;
`

function BackButton(props) {
    return (
        <StyledBackButton>
            <FontAwesomeIcon icon={faArrowLeft} />
        </StyledBackButton>
    );

}

export default BackButton;