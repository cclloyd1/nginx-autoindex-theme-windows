import styled from '@emotion/styled'
import {faArrowRight} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import NavButtonBase from './NavButtonBase';



const StyledBackButton = styled(NavButtonBase)`
`

function BackButton(props) {
    return (
        <StyledBackButton>
            <FontAwesomeIcon icon={faArrowRight}/>
        </StyledBackButton>
    );

}

export default BackButton;