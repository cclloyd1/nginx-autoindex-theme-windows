import {faDownload} from '@fortawesome/free-solid-svg-icons';
import {useEffect, useState} from 'react';
import {useExplorer} from '../../ExplorerContext';
import {OptionBase} from './OptionBase';

export function OptionDownload({ file, index, context, openCallback, ...otherProps }) {
    const { fileTree } = useExplorer();
    const [url, setURL] = useState('#');

    useEffect(() => {
        let base = window.location;
        let url = base.protocol + "//" + base.host + "/" + base.pathname.split('/')[1];

        for(let i=0; i<fileTree.length; i++) {
            if (fileTree[i].active !== null) url = `${url}/${fileTree[i].files[fileTree[i].active].name}`;
        }
        url = `${url}/${file.name}`;
        setURL(url);
    }, []);


    return (
        <OptionBase
            href={url}
            download={file.name}
            as={'a'}
            title={`Download ${file.name}`}
            faIcon={faDownload}
            {...otherProps}
        />
    );

}
