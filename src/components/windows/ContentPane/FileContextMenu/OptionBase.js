import styled from '@emotion/styled'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useExplorer} from '../../ExplorerContext';


const ContextOption = styled('div')`
  color: white;
  &:hover {
    background-color: #414141;
    color: white;
  }
  display: flex;
  align-items: center;
  padding-right: 2em;
`

const ContextIcon = styled('div')`
  width: 32px;
  height: 22px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const ContextOptionName = styled('div')`
  flex-grow: 1;
  padding: 0 4px 0 0;
  height: 22px;
  display: flex;
  align-items: center;
  font-weight: ${props => props.bold ? 'bold' : 'inherit'};
`


const Icon = styled('img')`
  width: 16px;
  height: 16px;
`


export function OptionBase({ file, index, title, icon, faIcon, bold, ...otherProps }) {
    const { setMenuActive } = useExplorer();

    return (
        <ContextOption
            onClick={() => setMenuActive(null)}
            file={file}
            index={index}
            {...otherProps}
        >
            <ContextIcon>
                {faIcon && <FontAwesomeIcon icon={faIcon}/>}
                {icon && <Icon src={icon}/>}
            </ContextIcon>
            <ContextOptionName bold={bold}>{title}</ContextOptionName>
        </ContextOption>
    );

}
