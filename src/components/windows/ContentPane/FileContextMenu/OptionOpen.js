import {useExplorer} from '../../ExplorerContext';
import {OptionBase} from './OptionBase';

export function OptionOpen({ file, index, context, openCallback, ...otherProps }) {
    const { fileTree } = useExplorer();

    return (
        <OptionBase
            title={`Open`}
            file={file}
            index={index}
            css={{fontWeight: 'bold'}}
            {...otherProps}
            bold
        />
    );

}
