import styled from '@emotion/styled'
import {useExplorer} from '../../ExplorerContext';
import {OptionDownload} from './OptionDownload';
import {OptionOpen} from './OptionOpen';

const menuWidth = 200;

const StyledDiv = styled.div`
  position: fixed;
  top: ${props => props.y}px;
  left: ${props => props.x}px;
  background-color: #2b2b2b;
  border: 1px solid #a0a0a0;
  min-width: ${menuWidth}px;
  min-height: 20px;
  z-index: 10000;
  box-shadow: 3px 3px 3px rgba(0,0,0,.25);
  padding: 4px 2px;
`

export function FileContextMenu({ file, index, context, openCallback }) {
    const { posX, posY } = useExplorer();

    const handleContextMenu = (e) => {
        e.stopPropagation();
    }

    const handleClick = (e) => {
        e.stopPropagation();
    }


    return (
        <StyledDiv
            x={context.clientX - posX - menuWidth}
            y={context.clientY - posY}
            className={'fileContextMenu'}
            onContextMenu={handleContextMenu}
            onClick={handleClick}
            onDoubleClick={handleClick}
        >

            {file.isDir && <OptionOpen file={file} index={index}/>}
            {!file.isDir && <OptionDownload file={file} index={index}/>}

        </StyledDiv>
    );

}
