import ColumnHead from '../ColumnHead';


export function ColumnName({ title }) {
    const sortID = 'name';

    return (
        <ColumnHead title={'Name'} sortID={sortID} />
    );
}
