import ColumnHead from '../ColumnHead';


export function ColumnSize({ title }) {
    const sortID = 'size';

    return (
        <ColumnHead title={'Size'} sortID={sortID} />
    );
}
