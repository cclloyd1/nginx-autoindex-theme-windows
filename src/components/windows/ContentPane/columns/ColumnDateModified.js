import ColumnHead from '../ColumnHead';


export function ColumnDateModified({ title }) {
    const sortID = 'dateModified';

    return (
        <ColumnHead title={'Date Modified'} sortID={sortID} />
    );
}
