import styled from '@emotion/styled'
import {useExplorer} from '../ExplorerContext';


const StyledFileListDetails = styled.table`
  color: white;
  border-spacing: 0;
  border-collapse: collapse;
`

const StyledRow = styled.tr`
  color: #eee;
  cursor: default;
  user-select: none;
  font-size: 11pt;
  &:hover {
    background-color: #4d4d4d;
  }
`
const StyledDiv = styled.div`
  overflow: auto;
  height: 100%;
  max-height: 100%;
`

const StyledCell = styled.td`
  padding-right: 1em;
`

function FileListDetails() {

    const { fileTree } = useExplorer();

    return (
        <StyledDiv>
            <StyledFileListDetails>
                <tbody>
                    {fileTree.map((f, i) => <StyledRow key={i}>
                        <StyledCell>{f.name}</StyledCell>
                        <StyledCell>{f.size}</StyledCell>
                        <StyledCell>{f.date}</StyledCell>
                    </StyledRow>)}
                </tbody>
            </StyledFileListDetails>
        </StyledDiv>
    );

}

export default FileListDetails;