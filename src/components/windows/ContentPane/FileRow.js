import styled from '@emotion/styled'
import axios from 'axios';
import filesize from 'filesize';
import {useEffect, useRef, useState} from 'react';
import {getFileIcon} from '../../../lib/icons';
import {useExplorer} from '../ExplorerContext';
import {faArrowTurnUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {FileContextMenu} from './FileContextMenu/FileContextMenu';



const StyledRow = styled.tr`
  color: #eee;
  cursor: ${props => props.fetching ? 'wait' : 'default'};
  user-select: none;
  font-size: 13px;
  //position: relative;
  
  //scroll-snap-align: center;
  white-space: nowrap;
  overflow: hidden;
  background-color: ${props => props.active ? '#838383' : 'transparent'};
  &:hover {
    background-color: ${props => props.active ? '#838383' : '#4d4d4d'};
  }
  &:active {
    background-color: #838383;
  }
`
const StyledCell = styled.td`
`

const StyledCellNew = styled.div`
  padding-right: 1em;
  display: flex;
  align-items: center;
`


const FileIcon = styled.img`
  width: 16px;
  height: auto;
  padding-right: 5px;
`

const StyledFontIcon = styled(FontAwesomeIcon)`
  font-size: 11px;
`


function FileRow({ file, index }) {
    const { getFilesFromIndex, setFileTree, fileTree, menuActive, setMenuActive } = useExplorer();

    const [active, setActive] = useState(false);
    const [context, setContext] = useState(null);
    const [fetching, setFetching] = useState(false);

    const menu = useRef(null);

    const handleClick = (e) => {
        e.stopPropagation();
        setActive(!active);
        if (menuActive) setMenuActive(null);
    }

    const handleContextMenu = (e) => {
        setActive(true);
        setMenuActive({index: index, file: file});
        setContext(e);
    }

    useEffect(() => {
        if (index !== menuActive?.index && file.name !== menuActive?.file.name) {
            setContext(null);
            setActive(false);
        }
    }, [menuActive])

    const handleDoubleClick = async () => {
        if (file.isDir) {
            if (file.name === '..') {
                setFileTree([
                    ...fileTree.slice(0, -2),
                    { ...fileTree.at(-2), active: null },
                ]);
                return;
            }

            let base = window.location;
            let url = base.protocol + "//" + base.host + "/" + base.pathname.split('/')[1];

            for(let i=0; i<fileTree.length; i++) {
                if (fileTree[i].active !== null) url = `${url}/${fileTree[i].files[fileTree[i].active].name}`;
            }
            url = `${url}/${file.name}`;

            try {
                setFetching(true)
                const res = await axios.get(url);
                fileTree.at(-1).active = index;
                setFileTree([...fileTree, {
                    files: getFilesFromIndex(res.data),
                    active: null,
                    name: fileTree.at(-1).files[index].name,
                }]);
                setMenuActive(null);
                setFetching(false);
            } catch (error) {
                console.error(error);
                setFetching(false);
            }
        }
    }

    if (file.name === '..') return (<></>);


    return (
        <StyledRow
            onClick={handleClick}
            active={active}
            fetching={fetching}
            onDoubleClick={() => handleDoubleClick().then()}
            onContextMenu={handleContextMenu}
        >
            <td>
                <StyledCellNew>
                    {context !== null && <FileContextMenu file={file} index={index} context={context} openCallback={handleDoubleClick}/>}
                    {file.isDir ? <FileIcon src={`/icons/shell32/shell32-5.ico`} /> : <FileIcon src={getFileIcon(file)} />}
                    {file.name === '../' ? <StyledFontIcon icon={faArrowTurnUp} /> : file.name}
                </StyledCellNew>
            </td>
            <StyledCell>{file.size >= 0 ? filesize(file.size) : '-'}</StyledCell>
            <StyledCell>{file.date}</StyledCell>
        </StyledRow>
    );

}

export default FileRow;