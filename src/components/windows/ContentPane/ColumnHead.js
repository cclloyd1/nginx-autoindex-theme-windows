import {css} from '@emotion/css';
import styled from '@emotion/styled'
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useState} from 'react';
import {useExplorer} from '../ExplorerContext';


const StyledTh = styled.th`
  height: 25px;
  font-size: 9pt;
  font-weight: normal;
  min-width: 100px;
  text-align: left;
  background-color: #202020;
  color: white;
  padding: 0 0 0 7px;
  position: sticky;
  top: 0;
  white-space: nowrap;
  overflow: hidden;
  &:hover {
    background-color: #434343;
  }
  &:active {
    background-color: #838383;
  }
`
const StyledContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  height: 100%;
  width: 100%;
  border-right: 1px solid ${props => props.hovered ? 'transparent' : '#636363'};
  position: relative;
  z-index: 10;
`


const StyledButton = styled.div`
  font-size: 8px;
  color: #e8e8e8;
  align-items: center;
  justify-content: center;
  height: 25px;
  width: 15px;
  border-left: 1px solid #565656;
  &:hover {
    color: white;
  }
  display: ${props => props.hovered ? 'flex' : 'none'};
`

const StyledDragHandle = styled.div`
  position: absolute;
  top: 0;
  right: -3px;
  width: 6px;
  height: 100%;
  cursor: ew-resize;
`
const StyledSortIcon = styled(FontAwesomeIcon)`
  position: absolute;
  top: 0;
  right: calc(50% - 3.5px);
  color: #878787;
  font-size: 8px;
`

export default function ColumnHead({ title, sortID }) {
    const [hovered, setHovered] = useState(false);

    const { sort, handleSort, sortDir } = useExplorer();
    //const [sort, setSort] = useState(null);

    const handleClick = () => {
        handleSort(sortID);
    }

    return (
        <StyledTh
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
            onClick={handleClick}
        >
            <StyledContainer hovered={hovered}>
                {sort === sortID && <StyledSortIcon icon={sortDir === 'asc' ? faChevronUp : faChevronDown} />}
                <div>{title}</div>
                <StyledButton hovered={hovered}>
                    <FontAwesomeIcon icon={faChevronDown} />
                </StyledButton>
                <StyledDragHandle/>
            </StyledContainer>
        </StyledTh>
    );
}
