import styled from '@emotion/styled'
import {useExplorer} from '../ExplorerContext';
import {ColumnDateModified} from './columns/ColumnDateModified';
import {ColumnName} from './columns/ColumnName';
import {ColumnSize} from './columns/ColumnSize';
import FileRow from './FileRow';


const StyledDiv = styled.div`
  padding-left: 15px;
  height: 100%;
  max-height: 100%;
  overflow: auto;
  scroll-snap-type: y proximity;
  //scroll-padding: 6px;
`

const StyledTable = styled.table`
  padding: 0;
  margin: 0;
  border-collapse: collapse;
  border-width: 0;
  //position: relative;
  user-select: none;
`

export default function ContentPane() {

    const { fileTree } = useExplorer();

    return (
        <StyledDiv>
            <StyledTable>
                <thead>
                    <tr>
                        <ColumnName/>
                        <ColumnSize/>
                        <ColumnDateModified/>
                    </tr>
                </thead>

                <tbody>
                    {fileTree.length > 0 && fileTree.at(-1)?.files?.map((file, i) => <FileRow file={file} key={i} index={i} />)}
                </tbody>
            </StyledTable>

        </StyledDiv>
    );

}

