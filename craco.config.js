const fs = require('fs');
const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = relativePath => path.resolve(appDirectory, relativePath);


module.exports = (config) => {

    const newConfig = {
        devServer: (devServerConfig, {env, paths, proxy, allowedHost}) => {
            devServerConfig.devMiddleware.writeToDisk = true;
            return devServerConfig;
        },
    }


    if (config.env === 'production') {
        console.log('Using production config');
        newConfig.output = {
            filename: '[name].js',
        }
    }
    else {
        console.log('Using development config');
    }

    return newConfig;
};

/*
module.exports = {
    devServer: (devServerConfig, { env, paths, proxy, allowedHost }) => {
        devServerConfig.devMiddleware.writeToDisk = true;
        return devServerConfig;
    },
    output: {
        filename: 'autoindex.js',
    },
    //appHtml: resolveAppPath('public/header.html')
    //plugins: [
    //    new CopyPlugin({
    //        patterns: [
    //            { from: resolveAppPath('public/assets'), to: resolveAppPath('build') },
    //        ],
    //    }),
    //]
};*/